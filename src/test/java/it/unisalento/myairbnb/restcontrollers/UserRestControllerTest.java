package it.unisalento.myairbnb.restcontrollers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.unisalento.myairbnb.entities.User;
import it.unisalento.myairbnb.service.UserService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserRestController.class)
public class UserRestControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	
	@MockBean
	private UserService userServiceMock;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	User user;
	
	private void setUp() {
		
		user = new User();
		
		user.setName("node_di_prova");
		user.setSurname("cognome_di_prova");
		user.setEmail("email_di_prova@email.com");
		
	}
	
	@Test
	public void getByIdReturnOkTest() throws Exception {
		
		mockMvc.perform(get("/user/getById/3")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
		
	}
	

	@Test
	public void saveReturnOk() throws JsonProcessingException, Exception {
		
		
		
		mockMvc.perform(post("/user/save")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				;
		
		
	}
	
	
}
